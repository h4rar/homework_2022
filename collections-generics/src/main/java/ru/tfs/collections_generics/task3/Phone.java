package ru.tfs.collections_generics.task3;

import lombok.*;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
@Data
@Builder
public class Phone {

    private String number;

    private PhoneType type;
}
