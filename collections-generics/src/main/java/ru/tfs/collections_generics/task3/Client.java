package ru.tfs.collections_generics.task3;

import lombok.*;

import java.util.*;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
@Data
@AllArgsConstructor
public class Client {

    private UUID id;

    private String name;

    private int age;

    private Set<Phone> phones;
}
