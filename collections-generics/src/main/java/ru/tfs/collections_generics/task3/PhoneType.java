package ru.tfs.collections_generics.task3;

import lombok.*;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
@AllArgsConstructor
public enum PhoneType {
    LANDLINE("Стационарный"),
    MOBILE("Мобильный");

    @Getter
    private String value;
}
