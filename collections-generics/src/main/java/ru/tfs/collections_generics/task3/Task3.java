package ru.tfs.collections_generics.task3;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
public class Task3 {

    public static void main(String[] args) {
        List<Client> clients = getSourceData();

//        1. Рассчитать суммарный возраст для определенного имени.
        int sumAge = clients.stream()
                .filter(it -> it.getName().equals("Антон"))
                .mapToInt(Client::getAge).sum();

//        2. Получить Set, который содержит в себе только имена клиентов в порядке их упоминания в исходном массиве
        LinkedHashSet<String> names = clients.stream()
                .map(Client::getName)
                .collect(Collectors.toCollection(LinkedHashSet::new));

//        3. Узнать, содержит ли список хотя бы одного клиента, у которого возраст больше заданного числа.
        boolean b = clients.stream().anyMatch(it -> it.getAge() > 100);

//        4. Преобразовать массив в Map, у которой ключ - уникальный идентификатор, значение - имя.
//           Поддержать порядок, в котором клиенты добавлены в массив.
        Map<UUID, String> nameById = clients.stream()
                .collect(Collectors.toMap(
                        Client::getId,
                        Client::getName,
                        (u, v) -> u,
                        LinkedHashMap::new
                ));

//        5. Преобразовать массив в Map, у которой ключ - возраст, значение - коллекция клиентов с таким возрастом.
        Map<Integer, List<Client>> clientsByAge = clients.stream()
                .collect(Collectors.groupingBy(Client::getAge));

//        6. Получить строку, содержащую телефоны всех клиентов через запятую. Предусмотреть, что у клиента телефонов может и не быть.
        String numberStr = clients.stream()
                .filter(it -> it.getPhones() != null && !it.getPhones().isEmpty())
                .flatMap(it -> it.getPhones().stream().map(Phone::getNumber))
                .collect(Collectors.joining(", "));

//        7. Найти самого возрастного клиента, которой пользуется стационарным телефоном
        Client client = clients.stream()
                .filter(it -> it.getPhones().stream()
                        .map(Phone::getType)
                        .anyMatch(type -> type.equals(PhoneType.LANDLINE)))
                .reduce((c1, c2) -> c1.getAge() > c2.getAge() ? c1 : c2)
                .orElseThrow();
    }

    private static List<Client> getSourceData() {
        return List.of(
                new Client(UUID.randomUUID(), "Антон", 25,
                        Set.of(
                                Phone.builder().number("123").type(PhoneType.LANDLINE).build(),
                                Phone.builder().number("456").type(PhoneType.MOBILE).build()
                        )
                ),
                new Client(UUID.randomUUID(), "Антон", 60,
                        Set.of(
                                Phone.builder().number("789").type(PhoneType.LANDLINE).build(),
                                Phone.builder().number("101").type(PhoneType.MOBILE).build()
                        )
                ),
                new Client(UUID.randomUUID(), "Егор", 18,
                        Set.of(
                                Phone.builder().number("121").type(PhoneType.LANDLINE).build(),
                                Phone.builder().number("141").type(PhoneType.MOBILE).build()
                        )
                ),

                new Client(UUID.randomUUID(), "Анна", 98,
                        Set.of(
                                Phone.builder().number("151").type(PhoneType.LANDLINE).build()
                        )
                ),

                new Client(UUID.randomUUID(), "Тамара", 30,
                        Set.of(
                                Phone.builder().number("151").type(PhoneType.LANDLINE).build()
                        )
                ),

                new Client(UUID.randomUUID(), "Света", 18,
                        Set.of(
                                Phone.builder().number("151").type(PhoneType.LANDLINE).build()
                        )
                )
        );
    }
}
