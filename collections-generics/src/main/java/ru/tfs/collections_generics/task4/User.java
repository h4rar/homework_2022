package ru.tfs.collections_generics.task4;

import lombok.ToString;

import java.util.*;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
@ToString
public class User {

    private Long id;

    private String name;

    @ToString.Exclude
    private List<User> friends;

    public User(String name) {
        this.name = name;
        this.id = new Random().nextLong();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }
}
