package ru.tfs.collections_generics.task4.service;

import ru.tfs.collections_generics.task4.User;

import java.util.*;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
public class SearchServiceImpl implements SearchService {

    @Override
    public List<User> searchForFriendsInWidth(
            User me, String name
    ) {
        return baseSearch(me, name, true);
    }

    @Override
    public List<User> searchForFriendsInDepth(User me, String name) {
        return baseSearch(me, name, false);
    }

    public List<User> baseSearch(User me, String name, boolean inWidth) {
        Deque<User> deque = new ArrayDeque<>();
        deque.push(me);

        List<User> result = new ArrayList<>();
        List<Long> checkedUsersId = new ArrayList<>();
        while (!deque.isEmpty()) {
            final User user = inWidth ? deque.pollLast() : deque.pollFirst();
            if (!checkedUsersId.contains(user.getId())) {
                if (user.getName().equals(name)) {
                    result.add(user);
                }
                checkedUsersId.add(user.getId());
                user.getFriends().forEach(u -> {
                    if (!checkedUsersId.contains(u.getId())) {
                        deque.push(u);
                    }
                });
            }
        }
        return result;
    }
}
