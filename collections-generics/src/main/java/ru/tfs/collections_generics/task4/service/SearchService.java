package ru.tfs.collections_generics.task4.service;

import ru.tfs.collections_generics.task4.User;

import java.util.List;

/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
public interface SearchService {

    List<User> searchForFriendsInWidth(User me, String name);

    List<User> searchForFriendsInDepth(User me, String name);
}
