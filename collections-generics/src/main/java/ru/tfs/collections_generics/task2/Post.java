package ru.tfs.collections_generics.task2;

import lombok.*;

/**
 * Created by Kharchenko A.R. on 06.03.2022
 */
@Data
@AllArgsConstructor
public class Post {

    private String text;

    private Integer likesCount;
}
