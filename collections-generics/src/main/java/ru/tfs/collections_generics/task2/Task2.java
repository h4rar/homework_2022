package ru.tfs.collections_generics.task2;

import java.util.*;

public class Task2 {

    public List<Post> getTop10(List<Post> posts) {
        final int size = 10;
        PriorityQueue<Post> top10PriorityQueue = new PriorityQueue<>(size, Comparator.comparing(Post::getLikesCount));
        initPriorityQueue(top10PriorityQueue, posts, size);
        for (int i = size; i < posts.size(); i++) {
            final Post post = posts.get(i);
            add(top10PriorityQueue, post);
        }

        List<Post> top10 = new ArrayList<>();
        while (!top10PriorityQueue.isEmpty()) {
            top10.add(0, top10PriorityQueue.poll());
        }
        return top10;
    }

    private static void initPriorityQueue(PriorityQueue<Post> top10PriorityQueue, List<Post> posts, int size) {
        for (int i = 0; i < size; i++) {
            top10PriorityQueue.add(posts.get(i));
        }
    }

    private static void add(PriorityQueue<Post> top10PriorityQueue, Post post) {
        final Post postWithMinLikes = Objects.requireNonNull(top10PriorityQueue.peek());
        if (post.getLikesCount() > postWithMinLikes.getLikesCount()) {
            top10PriorityQueue.poll();
            top10PriorityQueue.add(post);
        }
    }
}



