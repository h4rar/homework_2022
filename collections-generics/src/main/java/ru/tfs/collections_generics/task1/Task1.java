package ru.tfs.collections_generics.task1;

import java.util.*;

public class Task1 {

    private static final String INVALID_WARNING_TEXT = "Предупреждение: передано невалидное значение. " +
            "Формат входных данных должен быть следующим: \"имя_игрока количество_очков\"";

    public static void main(String[] args) {
        List<String> testList =
                Arrays.asList("Ivan 5", "Petr 3", "Alex 10", "Petr 8", "Ivan 6", "Alex 5", "Ivan 1", "Petr 5",
                        "Alex 1"
                );
        showWinner(testList);
    }

    public static void showWinner(List<String> competitors) {
        try {
            String winnerName = null;
            int winnerPoint = 0;
            for (String competitor : competitors) {
                String[] splitCompetitor = competitor.split(" ");
                if (splitCompetitor.length < 1) {
                    System.out.println(INVALID_WARNING_TEXT);
                    continue;
                }
                String name = splitCompetitor[0];
                if (splitCompetitor.length < 2) {
                    System.out.println(String.format("Предупреждение: Не задано количество очков для игрока %s", name));
                    continue;
                }
                int point = Integer.parseInt(splitCompetitor[1]);
                if (winnerPoint < point) {
                    winnerName = name;
                    winnerPoint = point;
                }
            }
            System.out.println(winnerName);
        } catch (Exception ex) {
            System.out.println("Произошла ошибка при определении победителя");
        }
    }
}
