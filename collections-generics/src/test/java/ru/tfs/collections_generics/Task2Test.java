package ru.tfs.collections_generics;

import org.junit.jupiter.api.Test;
import ru.tfs.collections_generics.task2.*;

import java.util.*;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * Created by Kharchenko A.R. on 09.03.2022
 */
public class Task2Test {

    private static final int MILLION = 1_000_000;

    @Test
    public void test1() {
        List<Post> posts = getData();

        Task2 task2 = new Task2();
        assertEquals(getExpectedTop10(posts), task2.getTop10(posts));
    }

    public static List<Post> getData() {
        final Random random = new Random();
        return random.ints(MILLION, 1, MILLION).
                mapToObj(i -> new Post("", i)).
                collect(Collectors.toList());
    }

    public static List<Post> getExpectedTop10(List<Post> posts) {
        return posts.stream()
                .sorted(Comparator.comparingInt(Post::getLikesCount)
                        .reversed())
                .limit(10)
                .collect(Collectors.toList());
    }
}
