package ru.tfs.concurrency.task2.cureDeadLock;

import java.util.concurrent.locks.*;

/**
 * Created by Kharchenko A.R. on 13.03.2022
 */
public class Account {

    private int cacheBalance;

    private final Lock lock = new ReentrantLock(true);

    public Lock getLock() {
        return lock;
    }

    public Account(int cacheBalance) {
        this.cacheBalance = cacheBalance;
    }

    public void addMoney(int money) {
        this.cacheBalance += money;
    }

    public boolean takeOffMoney(int money) {
        if (this.cacheBalance < money) {
            return false;
        }

        this.cacheBalance -= money;
        return true;
    }

    public int getCacheBalance() {
        return cacheBalance;
    }

}
