package ru.tfs.concurrency.task2.cureDeadLock;

/**
 * Created by Kharchenko A.R. on 13.03.2022
 */
public class AccountThread implements Runnable {

    private final Account accountFrom;

    private final Account accountTo;

    private final int money;

    public AccountThread(Account accountFrom, Account accountTo, int money) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.money = money;
    }

    @Override
    public void run() {
        for (int i = 0; i < 4000; i++) {
            if (accountFrom.getLock().tryLock()) {
                if (accountTo.getLock().tryLock()) {
                    if (accountFrom.takeOffMoney(money)) {
                        accountTo.addMoney(money);
                    }
                } else {
                    accountFrom.getLock().unlock();
                }
            }
        }
    }
}
