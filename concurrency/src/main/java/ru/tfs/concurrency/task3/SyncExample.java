package ru.tfs.concurrency.task3;

import java.util.concurrent.Semaphore;

/**
 * Created by Kharchenko A.R. on 20.03.2022
 */
public class SyncExample {
    private static boolean[] WORKPLACE = new boolean[5];
    private static Semaphore SEMAPHORE = new Semaphore(5, true);

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 8; i++) {
            new Thread(new Employee(i)).start();
        }
    }

    public static class Employee implements Runnable {

        private int id;

        public Employee(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            try {
                SEMAPHORE.acquire();
                int parkingNumber = -1;

                synchronized (WORKPLACE) {
                    for (int i = 0; i < 5; i++) {
                        if (!WORKPLACE[i]) {
                            WORKPLACE[i] = true;
                            parkingNumber = i;
                            System.out.println("worker " + id + " occupy production machine ...");
                            break;
                        }
                    }
                }

                synchronized (WORKPLACE) {
                    WORKPLACE[parkingNumber] = false;
                }

                SEMAPHORE.release();
                System.out.println("worker " + id + " release production machine");
            } catch (InterruptedException e) {
            }
        }
    }
}
