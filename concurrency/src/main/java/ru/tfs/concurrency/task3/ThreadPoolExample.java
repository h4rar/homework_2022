package ru.tfs.concurrency.task3;

import java.util.concurrent.*;

/**
 * Created by Kharchenko A.R. on 12.03.2022
 */
public class ThreadPoolExample {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 8; i++) {
            Employee employee = new Employee(i);
            executor.execute(employee);
        }
        executor.shutdown();
    }

    public static class Employee implements Runnable {

        private int id;

        public Employee(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            workOnMachine(id);
        }

        private static void workOnMachine(int workerId) {
            try {
                System.out.println("worker " + workerId + " occupy production machine ...");
                Thread.sleep(2000);
                System.out.println("worker " + workerId + " release production machine");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
