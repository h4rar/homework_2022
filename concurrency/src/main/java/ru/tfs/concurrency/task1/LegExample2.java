package ru.tfs.concurrency.task1;

import java.util.concurrent.CompletableFuture;

/**
 * Created by Kharchenko A.R. on 12.03.2022
 */
public class LegExample2 implements Runnable {

    private final String name;

    public LegExample2(String name) {
        this.name = name;
    }

    @Override
    public void run() {
            while (true) {
                System.out.println(name);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
    }

    public static void main(String[] args) {
        CompletableFuture.allOf(
                CompletableFuture.runAsync(new LegExample2("left")),
                CompletableFuture.runAsync(new LegExample2("right"))
        ).join();
    }
}
