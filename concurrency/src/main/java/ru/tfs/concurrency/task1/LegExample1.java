package ru.tfs.concurrency.task1;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.*;

/**
 * Created by Kharchenko A.R. on 12.03.2022
 */
public class LegExample1 implements Runnable {

    private final String name;

    private Lock lock;

    public LegExample1(String name, Lock lock) {
        this.name = name;
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock){
            while (true) {
                lock.notify();
                System.out.println(name);
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        CompletableFuture.allOf(
                CompletableFuture.runAsync(new LegExample1("left", lock)),
                CompletableFuture.runAsync(new LegExample1("right", lock))
        ).join();
    }
}
