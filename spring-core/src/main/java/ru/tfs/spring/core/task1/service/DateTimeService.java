package ru.tfs.spring.core.task1.service;

public interface DateTimeService {

    String today();

    String todayISO();
}
