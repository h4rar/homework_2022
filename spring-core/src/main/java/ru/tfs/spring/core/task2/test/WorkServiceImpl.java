package ru.tfs.spring.core.task2.test;

import ru.tfs.spring.core.task2.Timed;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */

public class WorkServiceImpl implements WorkService {

    @Override
    @Timed
    public void work() {
        System.out.println("work");
    }

    @Override
    @Timed
    public void work1() {
        System.out.println("work1");
    }
}
