package ru.tfs.spring.core.task2;

import java.lang.annotation.*;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Timed {

}
