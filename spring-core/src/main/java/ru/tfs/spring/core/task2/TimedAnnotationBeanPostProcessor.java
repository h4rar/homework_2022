package ru.tfs.spring.core.task2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.*;
import org.springframework.stereotype.Component;
import ru.tfs.spring.core.task2.repository.SuperPuperRepo;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
@Component
public class TimedAnnotationBeanPostProcessor implements BeanPostProcessor {

    @Qualifier("superPuperRepo")
    private final SuperPuperRepo repository;

    private Map<String, Class> loggingBeans = new HashMap<>();

    public TimedAnnotationBeanPostProcessor(
            SuperPuperRepo repository
    ) {
        this.repository = repository;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Timed.class) ||
                Arrays.stream(beanClass.getMethods()).anyMatch(method -> method.isAnnotationPresent(Timed.class))) {
            loggingBeans.put(beanName, beanClass);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> originalBean = loggingBeans.get(beanName);
        if (originalBean != null) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(originalBean);
            enhancer.setCallback((MethodInterceptor)(obj, method, args, proxy) -> {
                Optional<Method> originalMethod = Arrays.stream(originalBean.getMethods())
                        .filter(method::equals)
                        .findFirst();

                if (originalBean.isAnnotationPresent(Timed.class) ||
                        originalMethod.isPresent() && originalMethod.get().getAnnotation(Timed.class) != null) {
                    System.out.println("Старт");
                    final long before = System.nanoTime();
                    final Object retVal = method.invoke(bean, args);
                    final long after = System.nanoTime();
                    final long time = after - before;
                    System.out.println(time);
                    System.out.println("Конец");

                    final String name = method.getName();
                    MethodMetricStat methodMetricStat = new MethodMetricStat(name, time, LocalDateTime.now());
                    repository.save(methodMetricStat);
                    return retVal;
                }
                return method.invoke(bean, args);
            });
            return enhancer.create();
        }
        return bean;
    }
}
