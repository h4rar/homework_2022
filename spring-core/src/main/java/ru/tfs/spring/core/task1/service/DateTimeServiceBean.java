package ru.tfs.spring.core.task1.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class DateTimeServiceBean implements DateTimeService {

    private final SimpleDateFormat simpleDateFormat;

    @Qualifier("defaultSimpleDateFormat")
    private final SimpleDateFormat defaultSimpleDateFormat;

    public DateTimeServiceBean(SimpleDateFormat simpleDateFormat, SimpleDateFormat defaultSimpleDateFormat) {
        this.simpleDateFormat = simpleDateFormat;
        this.defaultSimpleDateFormat = defaultSimpleDateFormat;
    }

    @Override
    public String today() {
        return simpleDateFormat.format(new Date());
    }

    @Override
    public String todayISO() {
        return defaultSimpleDateFormat.format(new Date());
    }
}
