package ru.tfs.spring.core.task2.repository;

import ru.tfs.spring.core.task2.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by Kharchenko A.R. on 17.04.2022
 */
public class SuperPuperRepo {

    private ConcurrentHashMap<String, List<MethodMetricStat>> methodMetricStatByName = new ConcurrentHashMap<>();

    public ConcurrentHashMap<String, List<MethodMetricStat>> getMethodMetricStatByName() {
        return methodMetricStatByName;
    }

    public void setMethodMetricStatByName(
            ConcurrentHashMap<String, List<MethodMetricStat>> methodMetricStatByName
    ) {
        this.methodMetricStatByName = methodMetricStatByName;
    }

    public void save(MethodMetricStat methodMetricStat) {
        final String methodName = methodMetricStat.getMethodName();
        if (methodMetricStatByName.containsKey(methodName)) {
            methodMetricStatByName.get(methodName).add(methodMetricStat);
        } else {
            methodMetricStatByName.put(methodName, new ArrayList<>(List.of(methodMetricStat)));
        }
    }

    public Map<String, List<MethodMetricStat>> findByDateTimeBetween(LocalDateTime from, LocalDateTime to) {
        Map<String, List<MethodMetricStat>> result = new ConcurrentHashMap<>();
        for (var entry : methodMetricStatByName.entrySet()) {
            final List<MethodMetricStat> collect = entry.getValue().stream()
                    .filter(it -> it.getDateTime().isAfter(from) && it.getDateTime().isBefore(to))
                    .collect(Collectors.toList());
            if (!collect.isEmpty()) {
                result.put(entry.getKey(), collect);
            }
        }
        return result;
    }
}
