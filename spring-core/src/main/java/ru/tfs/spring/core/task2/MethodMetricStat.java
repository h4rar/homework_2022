package ru.tfs.spring.core.task2;

import java.time.LocalDateTime;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
public class MethodMetricStat {

    /**
     * Наименование/идентификатор метода
     */
    private String methodName;

    /**
     * Время работы метода
     */
    private Long time;

    private LocalDateTime dateTime;

    public MethodMetricStat(String methodName, Long time, LocalDateTime dateTime) {
        this.methodName = methodName;
        this.time = time;
        this.dateTime = dateTime;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
