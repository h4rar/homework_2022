package ru.tfs.spring.core.task2.test;

import org.springframework.context.annotation.*;
import ru.tfs.spring.core.task2.*;
import ru.tfs.spring.core.task2.repository.SuperPuperRepo;

import java.util.*;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
@Configuration
public class SpringConfig {

    @Bean()
    public WorkServiceImpl workService() {
        return new WorkServiceImpl();
    }

    @Bean()
    public TimedAnnotationBeanPostProcessor timedAnnotationBeanPostProcessor() {
        return new TimedAnnotationBeanPostProcessor(superPuperRepo());
    }

    @Bean("listMethodMetricStat")
    public List<MethodMetricStat> listMethodMetricStat() {
        return Collections.synchronizedList(new ArrayList<>());
    }

    @Bean("superPuperRepo")
    public SuperPuperRepo superPuperRepo() {
        return new SuperPuperRepo();
    }

    @Bean()
    public MetricStatProviderImpl statProvider() {
        return new MetricStatProviderImpl(listMethodMetricStat(), superPuperRepo());
    }
}
