package ru.tfs.spring.core.task2;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
public interface MetricStatProvider {

    /**
     * Получить статистику по метрикам по всем методам за указанный период
     */
    List<MethodMetricStatDto> getTotalStatForPeriod(LocalDateTime from, LocalDateTime to);

    /**
     * Получить статистику по метрикам по указанному методу за указанный период
     */
    MethodMetricStatDto getTotalStatByMethodForPeriod(String method, LocalDateTime from, LocalDateTime to);
}
