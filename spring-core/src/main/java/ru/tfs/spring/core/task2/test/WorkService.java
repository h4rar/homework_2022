package ru.tfs.spring.core.task2.test;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
public interface WorkService {

    void work();

    void work1();
}
