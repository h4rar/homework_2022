package ru.tfs.spring.core.task1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.*;
import ru.tfs.spring.core.task1.service.*;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
@Configuration
@PropertySource("classpath:application.properties")
public class Config {

    private static final String DATE_FORMAT = "EEEE dd MMM yyyy";

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    @Value("${time.locale}")
    private String locale;

    @Bean("simpleDateFormat")
    @Scope(value = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat(DATE_FORMAT, new Locale(locale));
    }

    @Bean("defaultSimpleDateFormat")
    @Scope(value = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SimpleDateFormat getDefaultSimpleDateFormat() {
        return new SimpleDateFormat(DEFAULT_DATE_FORMAT);
    }

    @Bean("dateTimeService")
    @Scope(value = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public DateTimeService dateTimeService() {
        return new DateTimeServiceBean(getSimpleDateFormat(), getDefaultSimpleDateFormat());
    }
}
