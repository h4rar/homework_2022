package ru.tfs.spring.core.task2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.tfs.spring.core.task2.repository.SuperPuperRepo;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
@Service
public class MetricStatProviderImpl implements MetricStatProvider {

    @Qualifier("listMethodMetricStat")
    private final List<MethodMetricStat> methodMetricStat;

    @Qualifier("superPuperRepo")
    private final SuperPuperRepo repository;

    public MetricStatProviderImpl(
            List<MethodMetricStat> objList,
            SuperPuperRepo repository
    ) {
        this.methodMetricStat = objList;
        this.repository = repository;
    }

    @Override
    public List<MethodMetricStatDto> getTotalStatForPeriod(
            LocalDateTime from, LocalDateTime to
    ) {
        final Map<String, List<MethodMetricStat>> byDateTimeBetween = repository.findByDateTimeBetween(from, to);

        Map<String, MethodMetricStatDto> statByName = new HashMap<>();
        for (var entry : byDateTimeBetween.entrySet()) {
            MethodMetricStatDto dto = new MethodMetricStatDto(entry.getKey(), 1L, null, null, null);
            for (MethodMetricStat methodForPeriod : entry.getValue()) {
                final Long time = methodForPeriod.getTime();
                updateDto(dto, time);
            }
            statByName.put(entry.getKey(), dto);
        }
        return new ArrayList<>(statByName.values());
    }

    private MethodMetricStatDto updateDto(
            MethodMetricStatDto dto, Long time
    ) {
        dto.incrementInvocationsCount();
        if (dto.getMinTime() == null || dto.getMinTime() > time) {
            dto.setMinTime(time);
        }
        if (dto.getMaxTime() == null || dto.getMaxTime() < time) {
            dto.setMaxTime(time);
        }
        if (dto.getAverageTime() == null) {
            dto.setAverageTime(time);
        } else {
            dto.setAverageTime((dto.getAverageTime() + time) / 2);
        }
        return dto;
    }

    @Override
    public MethodMetricStatDto getTotalStatByMethodForPeriod(String method, LocalDateTime from, LocalDateTime to) {
        return getTotalStatForPeriod(from, to).stream()
                .filter(it -> it.getMethodName().equals(method))
                .findFirst()
                .orElseThrow();
    }
}
