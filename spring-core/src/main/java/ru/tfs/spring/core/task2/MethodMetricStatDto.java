package ru.tfs.spring.core.task2;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
public class MethodMetricStatDto {

    /**
     * Наименование/идентификатор метода
     */
    private String methodName;

    /**
     * Кол-во вызовов метода
     */
    private Long invocationsCount;

    /**
     * Минимальное время работы метода
     */
    private Long minTime;

    /**
     * Среднее время работы метода
     */
    private Long averageTime;

    /**
     * максимальное время работы метода
     */
    private Long maxTime;

    public MethodMetricStatDto() {
    }

    public MethodMetricStatDto(String methodName, Long invocationsCount, Long minTime, Long averageTime, Long maxTime) {
        this.methodName = methodName;
        this.invocationsCount = invocationsCount;
        this.minTime = minTime;
        this.averageTime = averageTime;
        this.maxTime = maxTime;
    }

    public void incrementInvocationsCount() {
        invocationsCount++;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Long getInvocationsCount() {
        return invocationsCount;
    }

    public void setInvocationsCount(Long invocationsCount) {
        this.invocationsCount = invocationsCount;
    }

    public Long getMinTime() {
        return minTime;
    }

    public void setMinTime(Long minTime) {
        this.minTime = minTime;
    }

    public Long getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(Long averageTime) {
        this.averageTime = averageTime;
    }

    public Long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Long maxTime) {
        this.maxTime = maxTime;
    }
}
