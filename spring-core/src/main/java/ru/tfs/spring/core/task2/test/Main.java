package ru.tfs.spring.core.task2.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tfs.spring.core.task2.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Kharchenko A.R. on 03.04.2022
 */
public class Main {

    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(SpringConfig.class);
        final WorkService bean = context.getBean(WorkService.class);

        bean.work1();
        bean.work();
        bean.work1();
        bean.work1();
        bean.work1();
        bean.work1();

        final MetricStatProvider statByDate = context.getBean("statProvider", MetricStatProvider.class);
        final LocalDateTime of = LocalDateTime.of(2012, 6, 30, 12, 00);
        final LocalDateTime of1 = LocalDateTime.of(2022, 6, 30, 12, 00);
        final List<MethodMetricStatDto> totalStatForPeriod = statByDate.getTotalStatForPeriod(of, of1);
    }
}
