package ru.tfs.spring.core.task1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.tfs.spring.core.task1.service.*;

import java.util.Scanner;

@Component
public class Main {

    private static final String TODAY_COMMAND = "today";

    private static final String TODAY_ISO_COMMAND = "today-iso";

    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(Config.class);
        final DateTimeService dateTimeService = context.getBean(DateTimeService.class);

        while (true) {
            Scanner sc = new Scanner(System.in);
            String command = sc.nextLine().trim();
            switch (command) {
                case TODAY_COMMAND:
                    System.out.println(dateTimeService.today());
                    break;
                case TODAY_ISO_COMMAND:
                    System.out.println(dateTimeService.todayISO());
                    break;
                default:
                    System.out.println(String.format(
                            "Такой команды не существует.\nДоступные команды:\n%s\n%s",
                            TODAY_COMMAND,
                            TODAY_ISO_COMMAND
                    ));
            }
        }
    }
}
